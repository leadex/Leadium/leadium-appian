package org.leadium.appian

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.SelenideElement
import com.codeborne.selenide.ex.ElementNotFound
import org.junit.jupiter.api.Assertions
import org.openqa.selenium.By
import org.openqa.selenium.Keys

/**
 * Method for autocomplete widget.
 * It waits for the search results, then sends a signal to press the 'Enter' key.
 * @receiver SelenideElement
 */
fun SelenideElement.autocompleteEnter(keysArrowDownCount: Int = 0) {
    if (this.has(Condition.attribute("aria-autocomplete", "list"))) {
        try {
            this.parent().find(By.xpath(".//span[contains(text(), 'available')]")).should(Condition.be(Condition.exist))
            Selenide.sleep(1_000L)
            if (keysArrowDownCount > 0)
                for (o in keysArrowDownCount downTo 0) {
                    this.sendKeys(Keys.ARROW_DOWN)
                }
            this.sendKeys(Keys.ENTER)
        } catch (e: ElementNotFound) {
            Assertions.fail<String>("No results found!")
        }
    }
}

