package org.leadium.appian.data.tags

class Listbox(
    label: String,
    withText: Boolean = false,
    labelCaseInsensitive: Boolean = false,
    last: Boolean = false,
    targetIndex: Int = 1
) :
    AppianTag(label, withText, labelCaseInsensitive, last, targetIndex) {

    override val tagName = "Dropdown"
    override val axis = "/../..//div[@role='listbox']"
}