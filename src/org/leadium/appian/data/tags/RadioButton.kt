package org.leadium.appian.data.tags

class RadioButton(
    label: String,
    withText: Boolean = false,
    labelCaseInsensitive: Boolean = false,
    last: Boolean = false,
    targetIndex: Int = 1
) :
    AppianTag(label, withText, labelCaseInsensitive, last, targetIndex) {

    override val tagName = "Radio Button"
    override val axis = "/preceding::input"
}