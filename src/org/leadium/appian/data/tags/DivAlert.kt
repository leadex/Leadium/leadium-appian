package org.leadium.appian.data.tags

class DivAlert(
    label: String,
    withText: Boolean = false,
    labelCaseInsensitive: Boolean = false,
    last: Boolean = false,
    targetIndex: Int = 1
) :
    AppianTag(label, withText, labelCaseInsensitive, last, targetIndex) {

    override val tagName = "Alert"
    override val axis = "/ancestor::div/div/div[@role=\"alert\"]"
}