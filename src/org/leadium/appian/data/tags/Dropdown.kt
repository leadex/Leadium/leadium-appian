package org.leadium.appian.data.tags

class Dropdown(
    label: String,
    withText: Boolean = false,
    labelCaseInsensitive: Boolean = false,
    last: Boolean = false,
    targetIndex: Int = 3
) :
    AppianTag(label, withText, labelCaseInsensitive, last, targetIndex) {

    override val tagName = "Dropdown"
    override val axis = "/following::div"
}