package org.leadium.appian.data.tags

/**
 * Abstract class of HTML web elements.
 */
abstract class AppianTag(
    val label: String,
    val withText: Boolean = false,
    val caseInsensitive: Boolean = false,
    val last: Boolean = false,
    val targetIndex: Int = 1
) {

    abstract val tagName: String
    abstract val axis: String

    fun labelIndex(last: Boolean): String {
        return if (last) "last()" else "1"
    }

    fun createXpath(): String {
        val u = label.toUpperCase()
        val l = label.toLowerCase()
        return with(this) {
            when (withText) {
                true ->
                    if (caseInsensitive) {
                        when (this) {
                            is DivAlert -> "//*[contains(translate(text(), '$u', '$l'), '$l')]${axis}"
                            is Dropdown -> "(.//span[contains(translate(text(), '$u', '$l'), '$l')])[${labelIndex(last)}]${axis}[${targetIndex}]"
                            is Listbox -> ".//span[contains(translate(text(), '$u', '$l'), '$l')][${labelIndex(last)}]${axis}[${targetIndex}]"
                            else -> ".//*[contains(translate(text(), '$u', '$l'), '$l')][${labelIndex(last)}]${axis}[${targetIndex}]"
                        }
                    } else {
                        when (this) {
                            is DivAlert -> "//*[contains(text(), '$label')]${axis}"
                            is Dropdown -> "(.//span[contains(text(), '$label')])[${labelIndex(last)}]${axis}[${targetIndex}]"
                            is Listbox -> ".//span[contains(text(), '$label')][${labelIndex(last)}]${axis}[${targetIndex}]"
                            else -> ".//*[contains(text(), '$label')][${labelIndex(last)}]${axis}[${targetIndex}]"
                        }
                    }
                false -> {
                    if (caseInsensitive) {
                        when (this) {
                            is DivAlert -> "//*[translate(text(), '$u', '$l')='$l']${axis}"
                            is Dropdown -> "(.//span[translate(text(), '$u', '$l')='$l'])[${labelIndex(last)}]${axis}[${targetIndex}]"
                            is Listbox -> ".//span[translate(text(), '$u', '$l')='$l'][${labelIndex(last)}]${axis}[${targetIndex}]"
                            else -> ".//*[translate(text(), '$u', '$l')='$l'][${labelIndex(last)}]${axis}[${targetIndex}]"
                        }
                    } else {
                        when (this) {
                            is DivAlert -> "//*[text()='$label']${axis}"
                            is Dropdown -> "(.//span[text()='$label'])[${labelIndex(last)}]${axis}[${targetIndex}]"
                            is Listbox -> ".//span[text()='$label'][${labelIndex(last)}]${axis}[${targetIndex}]"
                            else -> ".//*[text()='$label'][${labelIndex(last)}]${axis}[${targetIndex}]"
                        }
                    }
                }
            }
        }
    }
}