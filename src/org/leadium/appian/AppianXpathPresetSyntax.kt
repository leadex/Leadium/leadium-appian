package org.leadium.appian

import org.leadium.appian.data.tags.AppianTag

interface AppianXpathPresetSyntax {

    fun xpathByAppianTag(appianTag: AppianTag) : String {
        return appianTag.createXpath()
    }
}